const config = require("config")
const chalk = require("chalk")
const pad = require("pad")
const moment = require("moment")

const request = require("./request")(config.get("ycombinatorApi"))
const connector = require("./ycombinator")(request)

const DISPLAY_NEWS_COUNT = 100;
const SCORE_SIZE = 5;
const DATE_FORMAT = "D. M. YYYY"
const TIME_FORMAT = "HH:mm"

const printDate = (time) => chalk.red.dim(pad(moment(time).format(DATE_FORMAT), 11))
const printTime = (time) => chalk.redBright(moment(time).format(TIME_FORMAT))
const printTitle = (title) => chalk.white.bold(title.substr(0, 100))
const printUrl = (url) => chalk.white(url)
const printScore = (score) => chalk.white.dim(pad(score.toString(), SCORE_SIZE))

connector
  .getTopStories()
  .then((topIds) => new Promise((resolve, reject) => {
    let storyStack = []

    console.log(`Got [${topIds.length}] top ids, retrieving id by id until we have [${DISPLAY_NEWS_COUNT}] items of type 'story'`)

    const getNextItem = (i) => {
      if (storyStack.length >= DISPLAY_NEWS_COUNT || i >= topIds.length) {
        console.log(i >= topIds.length
          ? `Got to the end of ids, gathered [${storyStack.length}] stories.`
          : `Got enough stories, aborting.`
        )
        return resolve(storyStack);
      }

      connector.getStoryById(topIds[i]).then(story => {
        if (story.type === "story") {
          storyStack.push(story);
          console.log(`Story ${topIds[i]} is of the correct type, adding to the stack. There are now [${storyStack.length}] stories.`)
        } else {
          console.log(`Discarding ${topIds[i]}, as it is of type [${story.type}].`)
        }

        getNextItem(i + 1);
      })
    }

    getNextItem(0)
  }))
  .then((topStories) => {
    console.log("\n\n")

    topStories.forEach(story => {
      console.log(`${printDate(story.time * 1000)} ${printTime(story.time * 1000)}: ${printTitle(story.title)}`)
      console.log(`+${printScore(story.score)} ${printUrl(story.url || connector.getStoryUrl(story.id))}`)
      story.url && console.log(` ${" ".repeat(SCORE_SIZE)} ${printUrl(connector.getStoryUrl(story.id))}`)

      console.log("")
    })
  })