# Hacker News in your console

![epic screenshot](https://gitlab.com/martin30/whynotcombinator/-/raw/master/docs/screenshot.png)

Be a hipster while looking like you're actually doing something and read titles of current top hacker news stories in your console! 

## Install

`npm install`
`npm start`

Tested on node `v10.17.0`

## Personalize

Change the value of `DISPLAY_NEWS_COUNT` to read more, or less.