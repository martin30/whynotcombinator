const axios = require("axios")

module.exports = (baseUrl) => (endpoint) => {
  let url = baseUrl + endpoint

  return axios.get(url).then(res => res.data)
}