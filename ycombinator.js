module.exports = (request) => ({
  getTopStories() {
    return request("topstories.json")
  },
  getStoryById(id) {
    return request(`item/${id}.json`)
  },
  getStoryUrl(id) {
    return `https://news.ycombinator.com/item?id=${id}`
  }
})